#!/bin/sh

# Stop flask server
kill flask

# Exit virtual environment
deactivate
