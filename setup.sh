#!/bin/sh

# Install python dependences
ACTIVATE_FILE="./venv/bin/activate"
python3 -m venv venv && source $ACTIVATE_FILE
pip install -r requirements.txt

# Build
# Tailwind css
npx tailwindcss -i app/static/tailwind/main.css -o app/static/css/main.css
