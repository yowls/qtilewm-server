## 📖 Description
This proyect is a web application to interact with my [Qtile Window Manager RICE](https://gitlab.com/yowls/qtilewm-rice) <br>
In this way, to be able to graphically manage my own configuration 

<!-- ## 📝 Changelog -->

<br>

## 🚀 Installation
### Requirements
Install: python3, pip3, npm

### Procedure
Run [`setup.sh`](setup.sh)

This will:
+ Create a virtual environment called venv
+ Enter in the virtual environment
+ Install dependencies there

<br>

## ⚙️ Usage
+ Run [`start.sh`](start.sh) to start the flask server
+ Go to: `localhost:5000` in your browser

<br>

## 📜 License
This project is subject to [AGPLv3](https://gitlab.com/yowls/qtilewm-server/-/blob/main/LICENSE)
