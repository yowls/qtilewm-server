#!/bin/sh

# Restart
if [ $1 = "-r" ]; then
	echo "Restarting.."
	kill flask
	sleep 1
fi


# Enter the virtual environment
source ./venv/bin/activate || exit 1


# Set environment variables
# export FLASK_APP=app.py
export FLASK_ENV=development
# export FLASK_DEBUG=1


# Start server
flask run
