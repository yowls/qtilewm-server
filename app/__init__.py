from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("settings/index.html")


@app.route('/settings/')
@app.route('/settings/<string:page>/')
def settings(page="index"):
    return render_template("settings/" + page + ".html")


# TODO: set flask config
#  if __name__ == "__main__":
#      app.config['ENV'] = "development"
#      app.run(debug=True)
